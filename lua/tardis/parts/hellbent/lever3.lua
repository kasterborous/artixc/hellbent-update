local PART={}
PART.ID = "hellbentlever3"
PART.Name = "Hell Bent TARDIS Lever 3"
PART.Model = "models/doctorwho1200/hellbent/lever3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3.5

PART.SoundOff = "doctorwho1200/hellbent/leveroff.wav"
PART.SoundOn = "doctorwho1200/hellbent/leveron.wav"

TARDIS:AddPart(PART)