local PART={}
PART.ID = "hellbentlqdoors"
PART.Name = "Hell Bent TARDIS Living Quarters Doors"
PART.Model = "models/doctorwho1200/hellbent/lqdoors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5

PART.SoundOff = "doctorwho1200/hellbent/lqdoorsclose.wav"
PART.SoundOn = "doctorwho1200/hellbent/lqdoorsopen.wav"
PART.SoundPos = Vector(-218.576, 21.994, 43.638)

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end
end

TARDIS:AddPart(PART)