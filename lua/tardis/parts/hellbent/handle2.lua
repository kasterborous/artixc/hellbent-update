local PART={}
PART.ID = "hellbenthandle2"
PART.Name = "Hell Bent TARDIS Handle 2"
PART.Model = "models/doctorwho1200/hellbent/handle2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "doctorwho1200/hellbent/handle.wav"

TARDIS:AddPart(PART)