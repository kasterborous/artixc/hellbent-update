local PART={}
PART.ID = "hellbentswitch"
PART.Name = "Hell Bent TARDIS Switch"
PART.Model = "models/doctorwho1200/hellbent/switch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/hellbent/switch.wav"

TARDIS:AddPart(PART)