local PART={}
PART.ID = "hellbentrs7"
PART.Name = "Hell Bent TARDIS Rotary Switch 7"
PART.Model = "models/doctorwho1200/hellbent/rotaryswitch7.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hellbent/rotaryswitch.wav"

TARDIS:AddPart(PART)