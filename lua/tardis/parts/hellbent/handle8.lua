local PART={}
PART.ID = "hellbenthandle8"
PART.Name = "Hell Bent TARDIS Handle 8"
PART.Model = "models/doctorwho1200/hellbent/handle8.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "doctorwho1200/hellbent/handle.wav"

TARDIS:AddPart(PART)