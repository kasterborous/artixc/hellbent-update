local PART={}
PART.ID = "hellbentlever"
PART.Name = "Hell Bent TARDIS Dematerialization Lever"
PART.Model = "models/doctorwho1200/hellbent/lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3.5

PART.SoundOff = "doctorwho1200/hellbent/leveroff.wav"
PART.SoundOn = "doctorwho1200/hellbent/leveron.wav"

TARDIS:AddPart(PART)