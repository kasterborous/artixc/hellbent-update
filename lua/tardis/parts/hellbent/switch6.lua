local PART={}
PART.ID = "hellbentswitch6"
PART.Name = "Hell Bent TARDIS Switch 6"
PART.Model = "models/doctorwho1200/hellbent/switch6.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/hellbent/switch.wav"

TARDIS:AddPart(PART)