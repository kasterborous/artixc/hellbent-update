local PART={}
PART.ID = "hellbentrs9"
PART.Name = "Hell Bent TARDIS Rotary Switch 9"
PART.Model = "models/doctorwho1200/hellbent/rotaryswitch9.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7

PART.Sound = "doctorwho1200/hellbent/rotaryswitch.wav"

TARDIS:AddPart(PART)