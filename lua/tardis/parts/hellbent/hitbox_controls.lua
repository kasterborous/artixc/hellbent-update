
local transparency = 0
local third_person_sound = ""

--------------------------------------------------------------------------------
-- common for all hitboxes
--------------------------------------------------------------------------------
local PART={}
PART.AutoSetup = true
PART.Collision = true
PART.Animate = false
PART.Name = "Hell Bent TARDIS hitbox"
function PART:Initialize()
	self:SetColor(Color(255,255,255,transparency))
	self:SetCollisionGroup(COLLISION_GROUP_WORLD)
end

--------------------------------------------------------------------------------
PART.Model = "models/hunter/plates/plate025x05.mdl"
--------------------------------------------------------------------------------

PART.ID = "hellbent_hitbox_keyboard"
PART.Sound = "doctorwho1200/hellbent/keyboard.wav"
TARDIS:AddPart(PART)

PART.ID = "hellbent_hitbox_telepathic"
PART.Sound = "doctorwho1200/hellbent/telepathic.wav"
TARDIS:AddPart(PART)

--------------------------------------------------------------------------------
PART.Model = "models/hunter/plates/plate.mdl"
--------------------------------------------------------------------------------

PART.Sound = "doctorwho1200/hellbent/ball.wav"
PART.ID = "hellbent_hitbox_third_person1"
TARDIS:AddPart(PART)
PART.ID = "hellbent_hitbox_third_person2"
TARDIS:AddPart(PART)
PART.ID = "hellbent_hitbox_third_person3"
TARDIS:AddPart(PART)
PART.ID = "hellbent_hitbox_third_person4"
TARDIS:AddPart(PART)
PART.ID = "hellbent_hitbox_third_person5"
TARDIS:AddPart(PART)

--------------------------------------------------------------------------------
PART.Model = "models/hunter/plates/plate.mdl"
--------------------------------------------------------------------------------

PART.Sound = "doctorwho1200/hellbent/keyboard.wav"
PART.ID = "hellbent_hitbox_virtual_console"
TARDIS:AddPart(PART)

--------------------------------------------------------------------------------
PART.Model = "models/hunter/blocks/cube025x05x025.mdl"
--------------------------------------------------------------------------------
PART.Sound = nil

PART.ID = "hellbent_hitbox_music_1"
TARDIS:AddPart(PART)
PART.ID = "hellbent_hitbox_music_2"
TARDIS:AddPart(PART)
PART.ID = "hellbent_hitbox_music_3"
TARDIS:AddPart(PART)
PART.ID = "hellbent_hitbox_music_4"
TARDIS:AddPart(PART)
PART.ID = "hellbent_hitbox_music_5"
TARDIS:AddPart(PART)
PART.ID = "hellbent_hitbox_music_6"
TARDIS:AddPart(PART)
PART.ID = "hellbent_hitbox_music_7"
TARDIS:AddPart(PART)

--------------------------------------------------------------------------------
PART.Model = "models/hunter/plates/plate.mdl"
--------------------------------------------------------------------------------
PART.Sound = "doctorwho1200/hellbent/switch.wav"
PART.ID = "hellbent_hitbox_unassignedcontrol1"
TARDIS:AddPart(PART)

PART.Sound = "doctorwho1200/hellbent/wheel_switch.wav"
PART.ID = "hellbent_hitbox_unassignedcontrol2"
TARDIS:AddPart(PART)
PART.ID = "hellbent_hitbox_unassignedcontrol3"
TARDIS:AddPart(PART)

--------------------------------------------------------------------------------
PART.Model = "models/hunter/blocks/cube025x05x025.mdl"
--------------------------------------------------------------------------------
PART.Sound = "doctorwho1200/hellbent/switch.wav"
PART.ID = "hellbent_hitbox_unassignedcontrol4"
TARDIS:AddPart(PART)
PART.ID = "hellbent_hitbox_unassignedcontrol5"
TARDIS:AddPart(PART)

--------------------------------------------------------------------------------
-- roof hole fixed
--------------------------------------------------------------------------------
local PART={}
PART.ID = "hellbent_roof_patch"
PART.AutoSetup = true
PART.Collision = false
PART.Animate = false
PART.Name = "Hell Bent roof hole patch"
PART.Model = "models/hunter/plates/plate1x1.mdl"

function PART:Initialize()
	self:SetColor(Color(255,255,255,255))
	self:SetMaterial("models/doctorwho1200/hellbent/white")
end

TARDIS:AddPart(PART)

PART.ID = "hellbent_door_patch"
PART.Name = "Hell Bent door hole patch"
PART.Model = "models/hunter/plates/plate025x075.mdl"
TARDIS:AddPart(PART)

