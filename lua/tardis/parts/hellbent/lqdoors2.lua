local PART={}
PART.ID = "hellbentlqdoors2"
PART.Name = "Hell Bent TARDIS Living Quarters Doors 2"
PART.Model = "models/doctorwho1200/hellbent/lqdoors2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5

PART.SoundOff = "doctorwho1200/hellbent/lqdoorsclose.wav"
PART.SoundOn = "doctorwho1200/hellbent/lqdoorsopen.wav"
PART.SoundPos = Vector(-637.612, 286.013, 48.051)

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end
end

TARDIS:AddPart(PART)