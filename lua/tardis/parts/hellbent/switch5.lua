local PART={}
PART.ID = "hellbentswitch5"
PART.Name = "Hell Bent TARDIS Switch 5"
PART.Model = "models/doctorwho1200/hellbent/switch5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/hellbent/switch.wav"

TARDIS:AddPart(PART)