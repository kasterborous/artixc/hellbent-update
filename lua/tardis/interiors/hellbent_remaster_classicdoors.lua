--Hell Bent TARDIS Classic

local T={}
T.Base="rehellbent"
T.Name="HellBent Remaster (Classic doors)"
T.ID="rehellbentcl"
T.EnableClassicDoors = true

T.IsVersionOf = "rehellbent"

T.Interior={
	Portal = {
		pos = Vector(132.962, 37.101, 43),
		ang = Angle(0, -180, 0),
		width = 150,
		height = 200
	},
	Fallback={
		pos = Vector(115.385, 37.311, 0),
		ang = Angle(0, 90, 0),
	},
	Sounds={
		Door={
			enabled=true,
			open = "doctorwho1200/hellbent/door.wav",
			close = "doctorwho1200/hellbent/door.wav",
		},
	},
	Parts={
		hbmaindoors=false,
		intdoor = {
			model="models/artixc/hellbent/maindoors.mdl",
			ang = Angle(0, 0, 0),
		},
		door={
			posoffset = Vector(door_offset, 1.25, -44.75),
		},
	},
	Controls = {},
	IntDoorAnimationTime = 2,
}
T.Exterior={
    Portal={
        pos=Vector(19.5,0,52.22),
        ang=Angle(0,0,0),
        width=32,
        height=87,
		thickness = 25,
		inverted = true,
	},
	Parts={
		door={
			posoffset=Vector(-19.5,0,-52.2),
			angoffset=Angle(0,0,0),
		},
	},
}

TARDIS:AddInterior(T)

--		pos = Vector(132.962, 37.101, 52.5),