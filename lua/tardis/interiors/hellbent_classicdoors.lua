-- Hell Bent TARDIS

local T={}
T.Base="hellbent"
T.Name="Hell Bent TARDIS (Classic doors)"
T.ID="hellbentcl"

T.EnableClassicDoors = true

T.IsVersionOf = "hellbent"

T.Interior={
	Portal={
		pos= Vector(-19.045, 129.324, 43.59),
		ang=Angle(0,-90,0),
		width=65,
		height=88
	},
	Fallback={
		pos=Vector(-20.112, 108.763, 1),
		ang=Angle(0,0,0),
	},
	Sounds={
		Door={
			enabled=true,
			open = "doctorwho1200/hellbent/door.wav",
			close = "doctorwho1200/hellbent/door.wav",
		},
	},
	Parts={
		hellbentintdoors=false,
		intdoor = { model="models/doctorwho1200/hellbent/intdoors.mdl", },
		door={
			posoffset=Vector(0, 0, -2.6),
			matrixScale = Vector(1, 1, 0.97)
		},
	},
	Controls = {
		hellbentswitch2 = false,
	},
	IntDoorAnimationTime = 2,
}
T.Exterior={
	Portal={
		pos=Vector(16.76,0,52.22),
		ang=Angle(0,0,0),
		width=30,
		height=88
	},
	Parts={
		door={
			posoffset=Vector(-3,0,0),
			angoffset=Angle(0,0,0),
		},
		hellbent_door_patch = {
			pos=Vector(13, 0, 94.9),
			ang = Angle(90, 0, 0),
			matrixScale = Vector(0.5, 0.74, 1)
		}
	},
	CustomHooks = {
		door_patch = {
			"PostInitialize",
			function(self)
				local patch = self:GetPart("hellbent_door_patch")
				if not IsValid(patch) then return end

				patch:SetMaterial("models/doctorwho1200/hellbent/exterior3")
			end,
		},
	},
}

TARDIS:AddInterior(T)