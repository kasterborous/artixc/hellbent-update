-- 1996 Tardis

local T = {}
T.Base = "base"
T.Name = "HellBent Remaster"
T.ID = "rehellbent"

T.EnableClassicDoors = false

T.Versions = {
	randomize = true,
	allow_custom = true,
	randomize_custom = true,

	main = {
		classic_doors_id = "rehellbentcl",
		double_doors_id = "rehellbent",
	},
	other = {},
}

T.Interior = {
    RequireLightOverride = true,
    Model = "models/artixc/hellbent/interior.mdl",
    IdleSound = {
        {
            path = "doctorwho1200/hellbent/interior.wav",
            volume = 1
        },
    },
    LightOverride = {
        basebrightness = 0.01,
        nopowerbrightness = 0.003
    },
    Light={
        color=Color(255,255,255),
        pos=Vector(0,0,120),
        brightness=0.5
  
  },
  Lights={
    {
    color=Color(199,179,255),
    pos=Vector(118.811, 191.859, 25.107),
    brightness=0.1
    },
    {
		color=Color(199,179,255),
    pos=Vector(86.435, 246.352, 23.748),
    brightness=0.1
    }
},
Lamps = {
	main = {
        color = Color(255, 255, 255),
        texture = "effects/flashlight/square",
        fov = 170,
        distance = 300,
        brightness = 0,
        pos = Vector(0.605, 0.634, 180.937),
        ang = Angle(90, 90, 0),
    },
    blue1 = {
        color = Color(181, 232, 255),
        texture = "models/artixc/hellbent/effect/hellbentglow",
        fov = 150,
        distance = 300,
        brightness = 5,
        pos = Vector(-202.76, 65.452, 41.166),
        ang = Angle(180, -30, 0),
    },
	blue2 = {
        color = Color(181, 232, 255),
        texture = "models/artixc/hellbent/effect/hellbentglow",
        fov = 150,
        distance = 300,
        brightness = 5,
        pos = Vector(-202.76, 65.452, 80.7),
        ang = Angle(180, -30, 0),
    },
	vent1 = {
        color = Color(181, 232, 255),
        texture = "effects/flashlight/soft",
        fov = 30,
        distance = 300,
        brightness = 0.1,
        pos = Vector(-125.392, 74.381, 130.7),
        ang = Angle(90, 90, 0),
		shadows = true
    },
	vent2 = {
        color = Color(181, 232, 255),
        texture = "effects/flashlight/soft",
        fov = 30,
        distance = 300,
        brightness = 0.1,
        pos = Vector(-171.487, -6.088, 130.7),
        ang = Angle(90, 90, 0),
		shadows = true
    },
},
Portal={
    pos=Vector(-20.46802,159.1,43.59),
    ang=Angle(0,-90,0),
    width=35,
    height=88
},
Fallback={
    pos=Vector(153.591,473.41,26.70),
    ang=Angle(0,90,0),
},
    Screens = {
        {
            pos = Vector(44.5, -6.5, 39),
            ang = Angle(0, 84.5, 91),
            width = 227.75,
            height = 140,
            visgui_rows = 2,
            power_off_black = true
        }
    },
    Sequences = "default_sequences",
    Parts = {
        door = {
            model = "models/artixc/hellbent/hbcapdoor.mdl",
            posoffset = Vector(23.3,0,-49.5),
            angoffset = Angle(0,180,0)
        },
        hbfault = true,
        hbmaindoors = true,
        hbsidedoors = true,
		hbroundel = true,
		hbconsole = true,
		hbdoorwall = true,
--        mcghandbreak = {pos = Vector(0, 0, 0), ang = Angle(0, 0, 0), },
    },
    Controls = {
        --mcghandbreak = "teleport",
    },
    Tips = {},
    -- Interior.Tips are deprecated; should be deleted when the extensions update and
    -- replace with Interior.CustomTips, Interior.PartTips and Interior.TipSettings
    TipSettings = {
        view_range_min = 70,
        view_range_max = 90,
    },
    CustomTips = {
        --{ text = "Example", pos = Vector(0, 0, 0) },
    },
    PartTips = {
--        mcghandbreak = {pos = Vector(19.374, 8.537, 55.021), down = true},
    },
    Seats = {
        {
            pos = Vector(130, -96, -30),
            ang = Angle(0, 40, 0)
        },
        {
            pos = Vector(125, 55, -30),
            ang = Angle(0, 135, 0)
        }
    },
    BreakdownEffectPos = Vector(0, 0, 40),
}

T.Exterior={
    Model="models/artixc/hellbent/exterior.mdl",
    Mass=2000,
    DoorAnimationTime = 0.65,
    Portal={
        pos=Vector(19.5,0,52.22),
        ang=Angle(0,0,0),
        width=32,
        height=92,
		thickness = 25,
		inverted = true,
    },
    Fallback={
        pos=Vector(35,0,5.5),
        ang=Angle(0,0,0)
    },
    Light={
        enabled=false,
    },
    Sounds={
        Teleport={
            demat="doctorwho1200/hellbent/demat.wav",
            mat="doctorwho1200/hellbent/mat.wav"
        },
        Lock="doctorwho1200/baker/lock.wav", --TEMP 
        Door={
            enabled=true,
            open="doctorwho1200/hellbent/doorext_open.wav",
            close="doctorwho1200/hellbent/doorext_close.wav",
        },
        FlightLoop="doctorwho1200/hellbent/flight_loop.wav"
    },
    Parts={
                door={
                        model="models/artixc/hellbent/hbcapdoor.mdl",
                        posoffset=Vector(-19.5,0,-52.2),
                        angoffset=Angle(0,0,0),
                },
        vortex={
            model="models/doctorwho1200/toyota/2014timevortex.mdl",--TEMP 
            pos=Vector(0,0,50),
            ang=Angle(0,0,0),
            scale=10
        }
    },
    Teleport = {
        SequenceSpeed = 0.60,
        SequenceSpeedFast = 0.935,
        DematSequence = {
            255,
            200,
            150,
            100,
            70,
            50,
            20,
            0
        },
        MatSequence = {
            0,
            20,
            50,
            100,
            150,
            180,
            255
        }
    }
}

--begin
TARDIS:AddInterior(T)




--[[ Old code incoming

local T={}
T.Base="base"
T.Name="Hell Bent Remaster"
T.ID="rehellbent"

T.EnableClassicDoors = false

T.Versions = {
	randomize = true,
	allow_custom = true,
	randomize_custom = true,

	main = {
		classic_doors_id = "hellbentcl",
		double_doors_id = "hellbent",
	},
	other = {},
}

T.Interior={
	Model="models/doctorwho1200/hellbent/interior.mdl",
	Portal={
		pos=Vector(-20.46802,159.1,43.59),
		ang=Angle(0,-90,0),
		width=35,
		height=88
	},
	Fallback={
		pos=Vector(-20.46, 142, 1),
		ang=Angle(0,0,0),
	},
	Sounds={},
	ExitDistance=1100,
	IdleSound={
		{
			path="doctorwho1200/hellbent/interior.wav",
			volume=1
		}
	},
	Light={
		color=Color(255,255,255),
		pos=Vector(0,0,110),
		warncolor=Color(255,0,0),
		brightness=0.4
	},
	Lights={
		{
			color=Color(255,217,208),
			pos=Vector(231.704, 30.789, 80.97),
			warncolor=Color(150,0,0),
			brightness=1.5
		},
		{
			color=Color(255,188,225),
			pos=Vector(-21.74, -143.414, 86.401),
			warncolor=Color(150,0,0),
			brightness=0.8
		},
	},
	LightOverride = {
		basebrightness = 0.9,
		nopowerbrightness = 0.015
	},
	Screens = {
		{
			pos =  Vector(4.311, -133.222, 110.2),
			ang = Angle(0, 150, 90),
			width = 500,
			height = 280,
			visgui_rows = 3,
			power_off_black = false
		}
	},
	ScreensEnabled =  false,
	Parts={
		console = false,
		hellbentintdoors		= {},
		intdoor = false,
		door={
			model="models/doctorwho1200/hellbent/doors.mdl",
			posoffset=Vector(0,0,0),
			angoffset=Angle(0,180,0)
		},
		hellbentconsole			= {},
		hellbentbulbs			= {},
		hellbentcb				= {},
		hellbentdoorframe		= {},
		hellbentfr				= {},
		hellbenthandle			= {},
		hellbenthandle2			= {},
		hellbenthandle3			= {},
		hellbenthandle4			= {},
		hellbenthandle5			= {},
		hellbenthandle6			= {},
		hellbenthandle7			= {},
		hellbenthandle8			= {},
		hellbenthandle9			= {},
		hellbentlever			= {},
		hellbentlever2			= {},
		hellbentlever3			= {},
		hellbentlqdoors			= {},
		hellbentlqdoors2		= {},
		hellbentlqdoors3		= {},
		hellbentrotor			= {},
		hellbentrs				= {},
		hellbentrs2				= {},
		hellbentrs3				= {},
		hellbentrs4				= {},
		hellbentrs5				= {},
		hellbentrs6				= {},
		hellbentrs7				= {},
		hellbentrs8				= {},
		hellbentrs9				= {},
		hellbentsl				= {},
		hellbentsl2				= {},
		hellbentsl3				= {},
		hellbentsl4				= {},
		hellbentsl5				= {},
		hellbentsl6				= {},
		hellbentsl7				= {},
		hellbentsl8				= {},
		hellbentsl9				= {},
		hellbentsl10			= {},
		hellbentsl11			= {},
		hellbentsl12			= {},
		hellbentsl13			= {},
		hellbentsl14			= {},
		hellbentsl15			= {},
		hellbentsl16			= {},
		hellbentstasisswitch	= {},
		hellbentswitch			= {},
		hellbentswitch2			= {},
		hellbentswitch3			= {},
		hellbentswitch4			= {},
		hellbentswitch5			= {},
		hellbentswitch6			= {},
		hellbentswitch7			= {},
		hellbentswitch8			= {},
		hellbentswitch9			= {},
		hellbentswitch10		= {},
		hellbentswitch11		= {},
		hellbentswitch12		= {},
		hellbentswitch13		= {},
		hellbentswitch14		= {},
		hellbentwallsparts		= {},
		hellbentwhiteswitch		= {},

		hellbent_hitbox_keyboard 			= { scale = 0.55,	pos = Vector(38.7, 6.5, 38.6),			ang = Angle(17, 0, 0),		},
		hellbent_hitbox_telepathic 			= { scale = 0.55,	pos = Vector(-30.7, 0, 42.6),			ang = Angle(-17, 0, 0),		},
		hellbent_hitbox_third_person1 		= { scale = 1.7,	pos = Vector(-6, -29.1, 40.3),										},
		hellbent_hitbox_third_person2 		= { scale = 1.7,	pos = Vector(25.29, -27.75, 36.45),									},
		hellbent_hitbox_third_person3 		= { scale = 1.7,	pos = Vector(28.16, 27.26, 36.45),									},
		hellbent_hitbox_third_person4 		= { scale = 1.7,	pos = Vector(9.51, 38.3, 36.45),									},
		hellbent_hitbox_third_person5 		= { scale = 1.7,	pos = Vector(-4.43, 40.0, 36.45),									},
		hellbent_hitbox_virtual_console 	= { scale = 1.7,	pos = Vector(30.568, -23.794, 36.474),	ang = Angle(0, 30, 0),		},

		hellbent_hitbox_music_1 			= { scale = 0.3,	pos = Vector(23.1, 0, 41.8),										},
		hellbent_hitbox_music_2 			= { scale = 0.3,	pos = Vector(29.1, 0, 40.0),										},
		hellbent_hitbox_music_3 			= { scale = 0.3,	pos = Vector(-20.1, 0, 41.8),										},
		hellbent_hitbox_music_4 			= { scale = 0.3,	pos = Vector(9.5, -16.825, 42.5),		ang = Angle(0, 120, 0),		},
		hellbent_hitbox_music_5 			= { scale = 0.3,	pos = Vector(-9.5, 16.825, 42.5),		ang = Angle(0, -60, 0),		},
		hellbent_hitbox_music_6 			= { scale = 0.3,	pos = Vector(-9.5, -16.825, 42.5),		ang = Angle(0, 60, 0),		},
		hellbent_hitbox_music_7 			= { scale = 0.3,	pos = Vector(9.5, 16.825, 42.5),		ang = Angle(0, -120, 0),	},
		hellbent_hitbox_unassignedcontrol1 	= { scale = 1.2,	pos =  Vector(-11.009, 37.957, 37.355),	ang = Angle(0, 120, 0), 	},
		hellbent_hitbox_unassignedcontrol2 	= { scale = 0.8,	pos = Vector(-10.608, 34.368, 39.705),	ang = Angle(0, 120, 0), 	},
		hellbent_hitbox_unassignedcontrol3 	= { scale = 0.8,	pos = Vector(13.15, -36.074, 38.553),	ang = Angle(0, -60, 0), 	},
		hellbent_hitbox_unassignedcontrol4 	= { scale = 0.3,	pos = Vector(-3.161, -39.942, 37.99),	ang = Angle(0, -30, 0),		},
		hellbent_hitbox_unassignedcontrol5 	= { scale = 0.2,	pos = Vector(21.226, -16.178, 42.918),	ang = Angle(0, 30, 0),		},
	},
	Controls = {
		hellbentlever = "teleport",
		hellbentstasisswitch = "vortex_flight",
		hellbentlever2 = "handbrake",
		hellbent_hitbox_keyboard = "coordinates",
		hellbentfr = "fastreturn",
		hellbent_hitbox_music_1 = "music",
		hellbent_hitbox_music_2 = "music",

		hellbentlever3 = "power",
		hellbenthandle2 = "repair",
		hellbentsl11 = "redecorate",
		hellbent_hitbox_music_7 = "music",
		hellbent_hitbox_third_person3 = "thirdperson",
		hellbent_hitbox_third_person4 = "thirdperson",

		hellbentswitch = "door",
		hellbentrs3 = "doorlock",
		hellbentswitch2 = "hellbent_intdoors",
		hellbentwhiteswitch = "hads",
		hellbentrs4 = "isomorphic",
		hellbentrs5 = "isomorphic",
		hellbent_hitbox_third_person5 = "thirdperson",
		hellbent_hitbox_music_5 = "music",

		hellbent_hitbox_telepathic = "destination",
		hellbentswitch11 = "toggle_screens",
		hellbentrs6 = "interior_lights",
		hellbent_hitbox_music_3 = "music",

		hellbenthandle4 = "physlock",
		hellbenthandle3 = "engine_release",
		hellbentswitch8 = "door",
		hellbentrs9 = "doorlock",
		hellbenthandle5 = "spin_switch",
		hellbenthandle6 = "spin_toggle",
		hellbentsl12 = "teleport",
		hellbentsl13 = "float",
		hellbentsl14 = "flight",
		hellbentsl15 = nil, --second bottom
		hellbent_hitbox_third_person1 = "thirdperson",
		hellbent_hitbox_music_6 = "music",

		hellbentsl16 = "cloak",
		hellbenthandle7 = "spin_cycle",
		hellbentswitch7 = "toggle_screens",
		hellbent_hitbox_virtual_console = "virtualconsole",
		hellbent_hitbox_music_4 = "music",
		hellbent_hitbox_third_person2 = "thirdperson",

		hellbent_hitbox_unassignedcontrol5 = nil,
	},
	TipSettings = {
		view_range_min = 40,
		view_range_max = 55,
		style = "hellbent",
	},
	PartTips = {
		hellbentlever 						= { pos = Vector(38.326, -9.011, 38.918),	right = false,	down = true, },
		hellbentlever2 						= { pos = Vector(32.081, -1.961, 40.316),	right = false,	down = false, },
		hellbent_hitbox_keyboard 			= { pos = Vector(38.43, 6.662, 39.352), 	right = true,	down = true, },
		hellbentfr 							= { pos = Vector(23.945, 6.8, 45.031), 		right = false,	down = false, },
		hellbentstasisswitch 				= { pos = Vector(35.883, -5.3, 40.675), 	right = true,	down = true, },
		hellbent_hitbox_music_1				= nil,
		hellbent_hitbox_music_2				= nil,

		hellbentlever3 						= { pos = Vector(19.099, 33.185, 39.918),  	right = false,	down = true, },
		hellbenthandle2 					= { pos = Vector(12.532, 34.06, 40.555), 	right = true,	down = false, },
		hellbentsl11 						= { pos = Vector(9.116, 23.828, 42.98), 	right = true,	down = false, },
		hellbent_hitbox_music_7 			= nil,
		hellbent_hitbox_third_person3 		= { pos = Vector(30.479, 27.289, 38.835), 	right = false,	down = true, view_range_max = 50, },
		hellbent_hitbox_third_person4 		= nil, --{ pos = Vector(8.851, 40.073, 38.554), 	right = true,	down = true, },

		hellbentswitch 						= { pos = Vector(-22.379, 29.135, 40.128), 	right = true,	down = true, },
		hellbentswitch2 					= { pos = Vector(-23.939, 28.217, 40.071), 	right = true,	down = false,},
		hellbentrs3 						= { pos = Vector(-21.188, 31.079, 40.026),	right = false,	down = true, },
		hellbentwhiteswitch 				= { pos = Vector(-5.196, 25.136, 43.359),	right = false,	down = false, },
		hellbentrs4 						= { pos = Vector(-28.497, 26.89, 39.844),	right = true,	down = true, },
		hellbentrs5 						= nil,
		hellbent_hitbox_third_person5 		= { pos = Vector(-4.955, 40.864, 39.313),	right = false,	down = true, view_range_max = 50, },
		hellbent_hitbox_music_5 			= nil,

		hellbent_hitbox_telepathic 			= { pos = Vector(-30.457, -3.207, 42.062),	right = false,	down = false, },
		hellbentswitch11					= { pos = Vector(-39.181, -0.102, 39.004),	right = true,	down = true, },
		hellbentrs6 						= { pos = Vector(-39.409, 11.864, 39.293),	right = false,	down = true, },
		hellbent_hitbox_music_3 			= { pos = Vector(-22.528, -2.011, 44.107),	right = false,	down = false, },

		hellbenthandle4 					= { pos = Vector(-32.988, -25.585, 39.841),	right = false,	down = true, },
		hellbenthandle3						= { pos = Vector(-31.511, -23.197, 40.091),	right = false,	down = false, },
		hellbentswitch8 					= { pos = Vector(-13.607, -38.514, 38.875),	right = true,	down = true, },
		hellbentrs9 						= { pos = Vector(-26.883, -20.101, 42.069),	right = false,	down = false, },
		hellbenthandle5 					= { pos = Vector(-13.357, -23.037, 43.174),	right = true,	down = false, },
		hellbenthandle6						= { pos = Vector(-11.382, -25.981, 42.582),	right = true,	down = true, },
		hellbentsl12 						= { pos = Vector(-16.696, -21.467, 43.202),	right = false,	down = false, text = "Secondary Throttle", },
		hellbentsl13 						= { pos = Vector(-20.162, -27.934, 40.951),	right = false,	down = true, },
		hellbentsl14 						= { pos = Vector(-22.781, -33.071, 38.204),	right = false,	down = true, },
		hellbentsl15 						= nil, --second bottom
		hellbent_hitbox_third_person1		= { pos = Vector(-3.861, -27.23, 42.532), 	right = true,	down = false, view_range_max = 49, },
		hellbent_hitbox_music_6 			= nil,

		hellbentsl16 						= { pos = Vector(6.877, -29.742, 41.817),	right = true,	down = true, },
		hellbenthandle7 					= { pos = Vector(12.806, -22.283, 43.979),	right = true,	down = true, },
		hellbentswitch7 					= { pos = Vector(17.564, -32.371, 40.011),	right = true,	down = false, },
		hellbent_hitbox_virtual_console 	= { pos = Vector(30.123, -22.526, 40.483),	right = false,	down = false, },
		hellbent_hitbox_music_4 			= { pos = Vector(7.434, -19.228, 44.107), 	right = false,	down = false, },
		hellbent_hitbox_third_person2 		= { pos = Vector(27.339, -28.142, 39.02), 	right = true,	down = true, view_range_max = 50, },

	},
	IntDoorAnimationTime = 2,
}

T.Exterior={
	Model="models/artixc/hellbentr/exterior.mdl",
	Mass=5000,
	Portal={
		pos=Vector(13.76,0,52.22),
		ang=Angle(0,0,0),
		width=35,
		height=88
	},
	Fallback={
		pos=Vector(44,0,7),
		ang=Angle(0,0,0)
	},
	Light={
		enabled=false,
	},
	Sounds={
		Teleport={
			demat="doctorwho1200/hellbent/demat.wav",
			mat="doctorwho1200/hellbent/mat.wav"
		},
		Lock="doctorwho1200/hellbent/lock.wav",
		Door={
			enabled=true,
			open="doctorwho1200/hellbent/doorext_open.wav",
			close="doctorwho1200/hellbent/doorext_close.wav",
		},
		FlightLoop="doctorwho1200/hellbent/flight_loop.wav",
	},
	Parts={
		door={
			model="models/doctorwho1200/hellbent/doorsext.mdl",
			posoffset=Vector(0,0,0),
			angoffset=Angle(0,0,0)
		},
		vortex={
			model="models/doctorwho1200/toyota/2014timevortex.mdl",
			pos=Vector(0,0,50),
			ang=Angle(0,0,0),
			scale=10
		}
	},
	CustomHooks = {
		door_textures = {
			{
				["PowerToggled"] = true,
				["ToggleDoorReal"] = true,
			},
			function(self)
				local door = self:GetPart("door")
				local idoor = self.interior:GetPart("door")
				if not IsValid(door) or not IsValid(idoor) then return end

				local power = self:GetPower()
				local open = self:GetData("doorstatereal", false)

				local mat = open and power and "exterior2_lit" or "exterior2"
				mat = "models/doctorwho1200/hellbent/" .. mat

				door:SetMaterial(mat)
				idoor:SetMaterial(mat)
			end,
		},
	},
}


T.Interior.TextureSets = {
	["normal"] = {
		prefix = "models/doctorwho1200/hellbent/",
		{ "self", 2, "floor2" }, -- floor
		{ "self", 13, "screen" }, -- screen

		{ "self", 7, "roundel" }, -- roundel
		{ "intdoor", 1, "roundel" },
		{ "hellbentintdoors", 1, "roundel" },
		{ "hellbentlqdoors", 1, "roundel" },
		{ "hellbentlqdoors2", 1, "roundel" },
		{ "hellbentlqdoors3", 1, "roundel" },

		{ "hellbentcb", 1, "computerbankslights" }, -- computer banks
		{ "hellbentrotor", 0, "glass" },
		{ "hellbentconsole", 7, "metersr" },

		{ "hellbentbulbs", 0, "bulbflasha" },
		{ "hellbentbulbs", 1, "bulb" },
		{ "hellbentbulbs", 2, "bulboff" },
		{ "hellbentbulbs", 3, "bulbflashb" },
	},
	["poweroff"] = {
		prefix = "models/doctorwho1200/hellbent/",
		{ "self", 2, "floor2_nopower" }, -- floor
		{ "self", 13, "screen_off" }, -- screen

		{ "self", 7, "roundel_off" }, -- roundel
		{ "intdoor", 1, "roundel_off" },
		{ "hellbentintdoors", 1, "roundel_off" },
		{ "hellbentlqdoors", 1, "roundel_off" },
		{ "hellbentlqdoors2", 1, "roundel_off" },
		{ "hellbentlqdoors3", 1, "roundel_off" },

		{ "hellbentcb", 1, "computerbankslights_off" }, -- computer banks
		{ "hellbentrotor", 0, "glass_nopower" },
		{ "hellbentconsole", 7, "metersr_off" },

		{ "hellbentbulbs", 0, "bulboff" },
		{ "hellbentbulbs", 1, "bulboff" },
		{ "hellbentbulbs", 2, "bulboff" },
		{ "hellbentbulbs", 3, "bulboff" },
	},
	["warning"] = {
		prefix = "models/doctorwho1200/hellbent/",
		{ "self", 2, "floor2" }, -- floor
		{ "self", 13, "screen_warn" }, -- screen

		{ "self", 7, "pulsingred" }, -- roundel
		{ "intdoor", 1, "pulsingred" },
		{ "hellbentintdoors", 1, "pulsingred" },
		{ "hellbentlqdoors", 1, "pulsingred" },
		{ "hellbentlqdoors2", 1, "pulsingred" },
		{ "hellbentlqdoors3", 1, "pulsingred" },

		{ "hellbentcb", 1, "pulsingred" }, -- computer banks
		{ "hellbentrotor", 0, "glass" },
		{ "hellbentconsole", 7, "metersr" },

		{ "hellbentbulbs", 0, "bulbflashb_w" },
		{ "hellbentbulbs", 1, "bulbflasha_w2" },
		{ "hellbentbulbs", 2, "bulbflasha_w" },
		{ "hellbentbulbs", 3, "bulb_w" },
	},
}

T.Interior.CustomHooks = {
	power_textures = {
		{
			["PowerToggled"] = true,
			["HealthWarningToggled"] = true,
		},
		function(self)
			local power = self.exterior:GetData("power-state")
			local warning = self.exterior:GetData("health-warning", false)

			if not power then
				self:ApplyTextureSet("poweroff")
			elseif warning then
				self:ApplyTextureSet("warning")
			else
				self:ApplyTextureSet("normal")
			end
		end,
	},
}

TARDIS:AddInterior(T)]]